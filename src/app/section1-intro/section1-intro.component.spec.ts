import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Section1IntroComponent } from './section1-intro.component';

describe('Section1IntroComponent', () => {
  let component: Section1IntroComponent;
  let fixture: ComponentFixture<Section1IntroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Section1IntroComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Section1IntroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
