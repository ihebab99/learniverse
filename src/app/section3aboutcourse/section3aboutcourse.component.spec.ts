import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Section3aboutcourseComponent } from './section3aboutcourse.component';

describe('Section3aboutcourseComponent', () => {
  let component: Section3aboutcourseComponent;
  let fixture: ComponentFixture<Section3aboutcourseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Section3aboutcourseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Section3aboutcourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
