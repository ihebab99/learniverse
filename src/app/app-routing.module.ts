import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './authentification/register/register.component';
import { LoginComponent } from './authentification/login/login.component';
import { HomeComponent } from './home/home.component';
import { CourseListComponent } from './course-list/course-list.component';
import { AuthenticationGuardGuard } from './shared/authentication-guard.guard';

const routes: Routes = [
  { path:'home', component:HomeComponent},
   { path:'register', component:RegisterComponent},
   {path:'login', component:LoginComponent},
   {path:'courses', component:CourseListComponent , canActivate:[AuthenticationGuardGuard]},
   {path:'', redirectTo:"home",pathMatch:'full'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
