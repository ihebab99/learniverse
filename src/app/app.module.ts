import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './authentification/register/register.component';

import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './authentification/login/login.component';
import { AuthInterceptor } from './shared/auth-interceptor';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthService } from './shared/auth.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { Section1IntroComponent } from './section1-intro/section1-intro.component';
import { Section12coursesComponent } from './section12courses/section12courses.component';
import { Section3aboutcourseComponent } from './section3aboutcourse/section3aboutcourse.component';
import { Section4topsubjectComponent } from './section4topsubject/section4topsubject.component';
import { Section5aboutComponent } from './section5about/section5about.component';
import { Section6TeamComponent } from './section6-team/section6-team.component';
import { CourseListComponent } from './course-list/course-list.component';
import { AuthenticationGuardGuard } from './shared/authentication-guard.guard';
@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    Section1IntroComponent,
    Section12coursesComponent,
    Section3aboutcourseComponent,
    Section4topsubjectComponent,
    Section5aboutComponent,
    Section6TeamComponent,
    CourseListComponent,
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    JwtModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    ReactiveFormsModule 
  ],
  providers: [
    AuthenticationGuardGuard,
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
