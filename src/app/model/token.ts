import { TokenType } from "./enum/tokenType";
import { User } from "./user.model";

export class Token{
    id?:number;
    token: string;
    tokenType: TokenType;
    expired: boolean;
    revoked: boolean;
    user: User;
}