import { Gender } from "./enum/gender.enum";
import { Role } from "./enum/role.enum";

export class RegisterRequest{
    constructor(
     public  firstname : string,
     public lastname : string,
     public email : string,
     public password : string,
     public role : Role,
     public gender : Gender,
     public phone : string,
     public photo : string   
    ){}
    
}