import { User } from "./user.model";

export interface AuthenticationResponse{
    access_token:string;
    refresh_token:string;
}