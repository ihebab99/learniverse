import { Role } from "./enum/role.enum";
import { Token } from "./token";

export class User{
    id?:number;
    firstanme:string;
    lastname:string;
    email:string;
    role:Role;
    tokens: Token;
}