import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuardGuard implements CanActivate {
  constructor(private router: Router , private authService : AuthService){}

  canActivate() : boolean{
    if(this.authService.isLoggedIn()){
      return true;
    }
    return false;
  }



  
  // private isUserLoggedIn():boolean{
  //   if(this.authService.isLoggedIn()){
  //     return true;
  //   }
  //   this.router.navigate(['/courses']);
  //   console.log("login first");
  //   return false;
  // }
  
}
