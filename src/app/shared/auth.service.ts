import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { LoginRequest } from '../model/loginRequest';
import { Observable, tap } from 'rxjs';
import { RegisterRequest } from '../model/registerRequest';
import { AuthenticationResponse } from '../model/authentication.Response';
import { User } from '../model/user.model';
import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public  BASEURL = environment.apiUrl;
  private accessTokenKey = 'access_token';
  private refreshTokenKey = 'refresh_token';
  private authenticatedUserKey = 'authenticated_user'; // Clé pour stocker les informations de l'utilisateur authentifié
  private token: string;
  private loggedInUsername: string;
  private jwtHelper = new JwtHelperService();


  constructor(private http : HttpClient) { }

  // login(login:LoginRequest) : Observable<AuthenticationResponse>{
  //   return this.http.post<AuthenticationResponse>(`${this.BASEURL}/login`,login);
  // }

  login(request: LoginRequest): Observable<AuthenticationResponse> {
    return this.http.post<AuthenticationResponse>(`${this.BASEURL}/login`, request)
      .pipe(
        tap(response => this.storeTokens(response))
      );
  }


  register(register:RegisterRequest) : Observable<AuthenticationResponse>{
    return this.http.post<AuthenticationResponse>(`${this.BASEURL}/register`, register);
  }

  // refreshToken(): Observable<any> {
  //   return this.http.post(`${this.BASEURL}/refresh-token`, {});
  // }


  refreshToken(): Observable<AuthenticationResponse> {
    const refreshToken = this.getRefreshToken();
    return this.http.post<AuthenticationResponse>(`${this.BASEURL}/refresh-token`, {})
      .pipe(
        tap(response => this.storeAccessToken(response.access_token))
      );
  }

  storeTokens(response: AuthenticationResponse): void {
    this.storeAccessToken(response.access_token);
    this.storeRefreshToken(response.refresh_token);
  }

  storeAccessToken(token: string): void {
    localStorage.setItem(this.accessTokenKey, token);
  }

  getAccessToken(): string | null {
    return localStorage.getItem(this.accessTokenKey);
  }

  storeRefreshToken(token: string): void {
    localStorage.setItem(this.refreshTokenKey, token);
  }

  getRefreshToken(): string | null {
    return localStorage.getItem(this.refreshTokenKey);
  }

  clearTokens(): void {
    localStorage.removeItem(this.accessTokenKey);
    localStorage.removeItem(this.refreshTokenKey);
  }


  storeAuthenticatedUser(user: User): void {
    localStorage.setItem(this.authenticatedUserKey, JSON.stringify(user));
  }


    // Méthode pour récupérer les informations de l'utilisateur authentifié depuis le localStorage
    getAuthenticatedUser(): User | null {
      const userJson = localStorage.getItem(this.authenticatedUserKey);
      return userJson ? JSON.parse(userJson) : null;
    }

    clearAuthenticatedUser(): void {
      localStorage.removeItem(this.authenticatedUserKey);
    }

    logout(): void {
      this.clearTokens();
      this.clearAuthenticatedUser();
      // Vous pouvez également ajouter d'autres opérations de nettoyage ou de redirection après la déconnexion si nécessaire
    }

    isLoggedIn(){
      return !!localStorage.getItem('access_token');
    }

    public isUserLoggedIn():boolean{
      var token =this.getAccessToken;
      if(this.token != null && this.token !== ''){
        if(this.jwtHelper.decodeToken(this.token).sub != null || ''){
          if (!this.jwtHelper.isTokenExpired(this.token)) {
            this.loggedInUsername = this.jwtHelper.decodeToken(this.token).sub;
            return true;
          }
        }
      }else{
        this.logout();
        return false;
      }

    }





}
