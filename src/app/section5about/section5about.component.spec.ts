import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Section5aboutComponent } from './section5about.component';

describe('Section5aboutComponent', () => {
  let component: Section5aboutComponent;
  let fixture: ComponentFixture<Section5aboutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Section5aboutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Section5aboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
