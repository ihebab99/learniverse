import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Section4topsubjectComponent } from './section4topsubject.component';

describe('Section4topsubjectComponent', () => {
  let component: Section4topsubjectComponent;
  let fixture: ComponentFixture<Section4topsubjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Section4topsubjectComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Section4topsubjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
