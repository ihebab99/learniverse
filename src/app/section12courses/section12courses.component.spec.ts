import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Section12coursesComponent } from './section12courses.component';

describe('Section12coursesComponent', () => {
  let component: Section12coursesComponent;
  let fixture: ComponentFixture<Section12coursesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Section12coursesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Section12coursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
