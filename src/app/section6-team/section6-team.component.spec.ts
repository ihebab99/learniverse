import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Section6TeamComponent } from './section6-team.component';

describe('Section6TeamComponent', () => {
  let component: Section6TeamComponent;
  let fixture: ComponentFixture<Section6TeamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Section6TeamComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Section6TeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
