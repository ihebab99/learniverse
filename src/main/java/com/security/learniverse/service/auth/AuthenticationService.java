package com.security.learniverse.service.auth;

import com.security.learniverse.dto.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface AuthenticationService {

    /**
     * User registration
     * @param request
     * @return
     */
    AuthenticationResponse register(RegisterRequest request);

    /**
     * User authentication
     * @param request
     * @return
     */
    AuthenticationResponse login(AuthenticationRequest request);



    /**
     * Method for the refresh token generation
     * @param request
     * @param response
     * @throws IOException
     */
    void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
