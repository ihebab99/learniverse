package com.security.learniverse.service.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.security.learniverse.dto.*;
import com.security.learniverse.exceptions.EmailExistsException;
import com.security.learniverse.model.User;
import com.security.learniverse.repository.UserRepository;
import com.security.learniverse.token.Token;
import com.security.learniverse.token.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.security.learniverse.token.TokenType.BEARER;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Service
//@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {
    @Autowired
    private  UserRepository userRepository;
    @Autowired
    private  TokenRepository tokenRepository;
    @Autowired

    private  PasswordEncoder passwordEncoder;
    @Autowired

    private JwtService jwtService;
    @Autowired

    private  AuthenticationManager authenticationManager;





    @Override
    public AuthenticationResponse register(RegisterRequest request) {
        if(userRepository.existsByEmail(request.getEmail())){
            throw new EmailExistsException("l'email est déja exitant");
        }
        var user = User.builder()
                .firstname(request.getFirstname())
                .lastname(request.getLastname())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(request.getRole())
                .gender(request.getGender())
                .phone(request.getPhone())
                .photo(request.getPhoto())
                .build();

        var savedUser = userRepository.save(user);

        var jwtToken = jwtService.generateToken(user);
        var refreshToken = jwtService.generateRefreshToken(user);
        saveUsertoken(savedUser, jwtToken);

        return AuthenticationResponse.builder()
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .build();
    }




    /**
     * create the token
     * @param User
     * @param jwtToken
     */
    private void saveUsertoken(User User, String jwtToken) {
        var token = Token.builder()
                .user(User)
                .token(jwtToken)
                .tokenType(BEARER)
                .revoked(false)
                .expired(false)
                .build();
        tokenRepository.save(token);
    }


    /**
     * retrieve user token
     * @param user
     */
    private void revokeAllUserToken(User user){
        var validUserToken= tokenRepository.findAllValidTokenByUser(user.getId());
        if (validUserToken.isEmpty())
            return;
        validUserToken.forEach(token ->{
            token.setExpired(true);
            token.setRevoked(true);
                });
        tokenRepository.saveAll(validUserToken);
    }





    @Override
    public AuthenticationResponse login(AuthenticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        var user = userRepository.findByEmail(request.getEmail())
                .orElseThrow();
        var jwtToken = jwtService.generateToken(user);
        var refreshToken = jwtService.generateRefreshToken(user);

        revokeAllUserToken(user);
        saveUsertoken(user, jwtToken);


        return AuthenticationResponse
                .builder()
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .build();
    }






    @Override
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {

        final String authHeader = request.getHeader(AUTHORIZATION);
        final String refreshToken;
        final String userEmail;


        if(authHeader==null || !authHeader.startsWith("Bearer ")){
            return;
        }


        refreshToken=authHeader.substring(7);

        /**
         * extract the userEmail from the JWT token
         */

        userEmail= jwtService.extractUsername(refreshToken);
        if(userEmail !=null){

            var user = this.userRepository.findByEmail(userEmail)
                    .orElseThrow();

            if(jwtService.isTokenValid(refreshToken, user) ){
                var accessToken = jwtService.generateToken (user);
                revokeAllUserToken(user);
                saveUsertoken(user, accessToken);
                var authResponse = AuthenticationResponse.builder()
                        .accessToken(accessToken)
                        .refreshToken(refreshToken)
                        .build();
                new ObjectMapper().writeValue(response.getOutputStream(),authResponse);

            }
        }

    }
}
