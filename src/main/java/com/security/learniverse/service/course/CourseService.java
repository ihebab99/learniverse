package com.security.learniverse.service.course;

import com.security.learniverse.dto.CreateCourseRequest;
import com.security.learniverse.model.Course;

import java.util.List;

public interface CourseService {

    /**
     * get all courses available
     * @return
     */
    List<Course> getAllCourses();

    /**
     * create course
     * @param courseRequest
     * @return Course
     */
    Course createCourse(CreateCourseRequest courseRequest);

    /**
     * update course
     * @param courseRequest
     * @return Course
     */
    Course updateCourse(Integer id,CreateCourseRequest courseRequest);

    /**
     * find course by title
     * @param title
     * @return
     */
    //Course findCourseByTitle(String title);

    /**
     * delete course
     * @param id
     */
    void deleteCourseById(Integer id);
}
