package com.security.learniverse.service.course;

import com.security.learniverse.dto.CreateCourseRequest;
import com.security.learniverse.exceptions.CourseNotFoundException;
import com.security.learniverse.model.Course;
import com.security.learniverse.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {

    private CourseRepository courseRepository;

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Override
    public List<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    @Override
    public Course createCourse(CreateCourseRequest courseRequest) {
        Course course = Course.builder()
                .title(courseRequest.getTitle())
                .subject(courseRequest.getSubject())
                .description(courseRequest.getDescription())
                .price(courseRequest.getPrice())
                .dateCreated(new Date())
                .lastUpdated(new Date())
                .build();

        return courseRepository.save(course);
    }


    @Override
    public Course updateCourse(Integer id,CreateCourseRequest courseRequest) {
        Course course = courseRepository.findById(id).orElse(null);
        if (course ==  null){
            throw new CourseNotFoundException("course not found with ID: "+id);
        }
        course.setTitle(courseRequest.getTitle());
        course.setSubject(courseRequest.getSubject());
        course.setDescription(courseRequest.getDescription());
        course.setPrice(courseRequest.getPrice());
        course.setLastUpdated(new Date());

        return courseRepository.save(course);
    }


   /* @Override
    public Course findCourseByTitle(String title) {
        return courseRepository.findByTitle(title)
                .orElseThrow(()->new CourseNotFoundException("course with title ${title} not found"+title));
    }*/

    @Override
    public void deleteCourseById(Integer id) {
        courseRepository.deleteById(id);
    }
}
