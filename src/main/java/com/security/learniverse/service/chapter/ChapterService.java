package com.security.learniverse.service.chapter;

import com.security.learniverse.dto.CreateChapterRequest;
import com.security.learniverse.model.Chapter;
import com.security.learniverse.model.Course;

import java.util.List;

public interface ChapterService {

    /**
     * get all chapters
     * @return
     */
    List<Chapter> getAllChapters();

    /**
     * create Course
     * @param chapterRequest
     * @return
     */
    Chapter createChapter(CreateChapterRequest chapterRequest);

    /**
     * update course
     * @param id
     * @param chapterRequest
     * @return
     */
    Chapter updateChapter(Integer id, CreateChapterRequest chapterRequest);

    /**
     * delete course
     * @param id
     */
    void deleteChapterById(Integer id);
}
