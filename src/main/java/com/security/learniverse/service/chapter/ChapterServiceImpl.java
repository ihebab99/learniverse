package com.security.learniverse.service.chapter;

import com.security.learniverse.dto.CreateChapterRequest;
import com.security.learniverse.exceptions.ChapterNotFoundException;
import com.security.learniverse.model.Chapter;
import com.security.learniverse.repository.ChapterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ChapterServiceImpl implements ChapterService {


    private ChapterRepository chapterRepository;

    @Autowired
    public ChapterServiceImpl(ChapterRepository chapterRepository) {
        this.chapterRepository = chapterRepository;
    }

    @Override
    public List<Chapter> getAllChapters() {
        return chapterRepository.findAll();
    }

    @Override
    public Chapter createChapter(CreateChapterRequest chapterRequest) {
        Chapter chapter = Chapter.builder()
                .title(chapterRequest.getTitle())
                .level(chapterRequest.getLevel())
                .description(chapterRequest.getDescription())
                .dateCreated(new Date())
                .lastUpdated(new Date())
                .build();

        return chapterRepository.save(chapter);
    }

    @Override
    public Chapter updateChapter(Integer id, CreateChapterRequest chapterRequest) {
        Chapter chapter = chapterRepository.findById(id).orElse(null);
        if (chapter ==  null){
            throw new ChapterNotFoundException("chapter not found with ID: "+id);
        }
        chapter.setTitle(chapterRequest.getTitle());
        chapter.setLevel(chapterRequest.getLevel());
        chapter.setDescription(chapterRequest.getDescription());
        chapter.setLastUpdated(new Date());
        return chapterRepository.save(chapter);
    }

    @Override
    public void deleteChapterById(Integer id) {
        chapterRepository.deleteById(id);
    }

}
