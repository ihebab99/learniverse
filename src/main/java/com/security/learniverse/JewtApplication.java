package com.security.learniverse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JewtApplication {

    public static void main(String[] args) {
        SpringApplication.run(JewtApplication.class, args);

    }

//    @Bean
//    public CommandLineRunner commandLineRunner(
//        AuthenticationService service
//        ){
//        return args -> {
//            var admin = RegisterRequest.builder()
//                    .firstname("admin")
//                    .lastname("admin")
//                    .email("admin@gmail.com")
//                    .password("password")
//                    .role(ADMIN)
//                    .build();
//            System.out.println("admin token : " + service.register(admin).getAccessToken());
//
//            var manager = RegisterRequest.builder()
//                    .firstname("manager")
//                    .lastname("manager")
//                    .email("manager@gmail.com")
//                    .password("password")
//                    .role(MANAGER)
//                    .build();
//            System.out.println("manager token : " + service.register(manager).getAccessToken());
//        };
//    }



}
