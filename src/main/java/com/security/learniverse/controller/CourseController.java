package com.security.learniverse.controller;

import com.security.learniverse.dto.CreateCourseRequest;
import com.security.learniverse.model.Course;
import com.security.learniverse.service.course.CourseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/courses")
@CrossOrigin(value = "http://localhost:4200")
@PreAuthorize("hasRole('TEACHER')")
@Tag(name = "Courses")
public class CourseController {

    @Autowired
    private CourseService courseService;

    @Operation(
            description = "Get endpoint for GetAllCourses",
            summary = "This is a summary for GetAllCourses get endpoint",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            description = "Unauthorized / Invalid Token",
                            responseCode = "403"
                    )
            }

    )
    @GetMapping
    @PreAuthorize("hasAuthority('teacher:read')")
    public ResponseEntity<List<Course>> getAllCourses(){
        List<Course> courses = courseService.getAllCourses();
        return ResponseEntity.ok(courses);
    }

    @Operation(
            description = "Post endpoint for createCourse",
            summary = "This is a summary for createCourse post endpoint",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            description = "Unauthorized / Invalid Token",
                            responseCode = "403"
                    )
            }

    )
    @PostMapping
    @PreAuthorize("hasAuthority('teacher:create')")
    public ResponseEntity<Course> createCourse(@RequestBody CreateCourseRequest courseRequest){
        Course course= courseService.createCourse(courseRequest);
        return ResponseEntity.ok(course);
    }

 /*   @GetMapping("/{title}")
    @PreAuthorize("hasAuthority('teacher:read')")
    public ResponseEntity<Course> getCourseByTitle(@PathVariable String title){
        Course course = courseService.findCourseByTitle(title);
        if (course == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(course);
    }*/

    @Operation(
            description = "Put endpoint for updateCourse",
            summary = "This is a summary for updateCourse post endpoint",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            description = "Unauthorized / Invalid Token",
                            responseCode = "403"
                    )
            }

    )
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('teacher:update')")
    public ResponseEntity<Course> updateCourse(@PathVariable Integer id,@RequestBody CreateCourseRequest courseRequest){
        Course updatedCourse = courseService.updateCourse(id, courseRequest);
        if (updatedCourse == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(updatedCourse);
    }


    @Operation(
            description = "Delete endpoint for deleteCourse",
            summary = "This is a summary for deleteCourse delete endpoint",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            description = "Unauthorized / Invalid Token",
                            responseCode = "403"
                    )
            }

    )
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('teacher:delete')")
    public ResponseEntity<Void> deleteCourse(@PathVariable Integer id) {
        courseService.deleteCourseById(id);
        return ResponseEntity.noContent().build();
    }

}
