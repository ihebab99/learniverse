package com.security.learniverse.controller;

import com.security.learniverse.dto.CreateChapterRequest;
import com.security.learniverse.model.Chapter;
import com.security.learniverse.service.chapter.ChapterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/chapters")
@CrossOrigin(value = "http://localhost:4200")
@PreAuthorize("hasRole('TEACHER')")
@Tag(name = "Chapters")
public class ChapterController {

    @Autowired
    private ChapterService chapterService;

    @Operation(
            description = "Get endpoint for getAllChapters",
            summary = "This is a summary for getAllChapters get endpoint",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            description = "Unauthorized / Invalid Token",
                            responseCode = "403"
                    )
            }

    )
    @GetMapping
    @PreAuthorize("hasAuthority('teacher:read')")
    public ResponseEntity<List<Chapter>> getAllChapters(){
        List<Chapter> chapters = chapterService.getAllChapters();
        return ResponseEntity.ok(chapters);
    }



    @Operation(
            description = "Post endpoint for createChapter",
            summary = "This is a summary for createChapter post endpoint",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            description = "Unauthorized / Invalid Token",
                            responseCode = "403"
                    )
            }

    )
    @PostMapping
    @PreAuthorize("hasAuthority('teacher:create')")
    public ResponseEntity<Chapter> createChapter(@RequestBody CreateChapterRequest chapterRequest){
        Chapter chapter= chapterService.createChapter(chapterRequest);
        return ResponseEntity.ok(chapter);
    }


    @Operation(
            description = "Put endpoint for updateChapter",
            summary = "This is a summary for updateChapter post endpoint",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            description = "Unauthorized / Invalid Token",
                            responseCode = "403"
                    )
            }

    )
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('teacher:update')")
    public ResponseEntity<Chapter> updateChapter(@PathVariable Integer id, @RequestBody CreateChapterRequest chapterRequest){
        Chapter updateChapter = chapterService.updateChapter(id, chapterRequest);
        if(updateChapter == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(updateChapter);
    }


    @Operation(
            description = "Delete endpoint for deleteChapter",
            summary = "This is a summary for deleteChapter delete endpoint",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            description = "Unauthorized / Invalid Token",
                            responseCode = "403"
                    )
            }

    )
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('teacher:delete')")
    public ResponseEntity<Void> deleteChapter(@PathVariable Integer id) {
        chapterService.deleteChapterById(id);
        return ResponseEntity.noContent().build();
    }





}
