package com.security.learniverse.dto;

import com.security.learniverse.model.enums.Level;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateChapterRequest {
    private String title;
    private Level level;
    private String description;
    private Date dateCreated;
    private Date lastUpdated;
}
