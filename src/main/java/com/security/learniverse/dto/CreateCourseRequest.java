package com.security.learniverse.dto;

import com.security.learniverse.model.enums.Field;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateCourseRequest {

    private String title;
    private Field subject;
    private String description;
    private Double price;

}
