package com.security.learniverse.dto;

import com.security.learniverse.model.enums.Gender;
import com.security.learniverse.model.enums.Role;
import com.security.learniverse.model.enums.Section;
import jakarta.annotation.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {

    private String firstname;
    private String lastname;
    private String email;
    private String password;
    private Role role;
    private Gender gender;
    private String phone;
    private String photo;







}
