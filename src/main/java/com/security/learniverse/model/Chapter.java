package com.security.learniverse.model;

import com.security.learniverse.model.enums.Level;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "chapter")
public class Chapter {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name="title")
    private String title;

    @Column(name="level")
    @Enumerated(EnumType.STRING)
    private Level level;

    @Column(name="description")
    private String description;


    @Column(name="date_created")
    private Date dateCreated;

    @Column(name="last_updated")
    private Date lastUpdated;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id")
    public Course course;

    @OneToOne(mappedBy = "chapter", cascade = CascadeType.ALL)
    private Resume resume;




}