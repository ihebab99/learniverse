package com.security.learniverse.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "teacher")
public class Teacher extends User{

    @Column(name = "speciality")
    private String speciality;

    @Column(name = "cin")
    private String cin;

    @Column(name = "year_of_experience")
    private String yearOfExperience;

    @Column(name = "institution_name")
    private String institutionName;
}
