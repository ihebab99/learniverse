package com.security.learniverse.model.enums;

public enum Field {
    PHYSICAL,
    CHEMISTRY,
    MATH,
    SCIENCE,
    TECHNOLOGY
}
