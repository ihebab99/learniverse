package com.security.learniverse.model.enums;

public enum Section {
    MATH,
    TECHNOLOGY,
    SCIENCES,
    ECONOMICSANDMANAGEMENT
}
