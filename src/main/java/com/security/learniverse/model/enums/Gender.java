package com.security.learniverse.model.enums;

public enum Gender {
    MALE,
    FEMALE
}
