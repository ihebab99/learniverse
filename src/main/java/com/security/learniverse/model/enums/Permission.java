package com.security.learniverse.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Permission {

    ADMIN_READ("admin:read"),
    ADMIN_UPDATE("admin:update"),
    ADMIN_CREATE("admin:create"),
    ADMIN_DELETE("admin:delete"),

    TEACHER_READ("teacher:read"),
    TEACHER_UPDATE("teacher:update"),
    TEACHER_CREATE("teacher:create"),
    TEACHER_DELETE("teacher:delete"),

    PUPIL_READ("pupil:read"),
    PUPIL_UPDATE("pupil:update"),
    PUPIL_CREATE("pupil:create"),
    PUPIL_DELETE("pupil:delete")
    ;
    @Getter
    private final String permission;

}
