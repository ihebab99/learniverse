package com.security.learniverse.model.enums;

public enum Level {
    EASY,
    INTERMEDIATE,
    ADVANCED
}
