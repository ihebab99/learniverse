package com.security.learniverse.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "resume")
public class Resume {
    @Id
    @GeneratedValue
    private Integer id;

    @Column(name="level")
    private String title;

    @Column(name="description")
    private String description;


    @Column(name="date_created")
    private Date dateCreated;

    @Column(name="last_updated")
    private Date lastUpdated;

    @OneToOne
    @JoinColumn(name = "chapter_id")
    private Chapter chapter;
}