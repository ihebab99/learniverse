package com.security.learniverse.model;

import com.security.learniverse.model.enums.Section;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "pupil")
public class Pupil extends User{

    @Column(name = "section")
    @Enumerated(EnumType.STRING)
    private Section section;

  /*  @Column(name = "enrollment_number")
    private String enrollmentNumber;*/
}
