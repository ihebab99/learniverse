package com.security.learniverse.model;

import com.security.learniverse.model.enums.Field;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "course")
public class Course {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name="title")
    private String title;

    @Enumerated(EnumType.STRING)
    private Field subject;

    @Column(name="description")
    private String description;

    @Column(name="price")
    private Double price;

    @Column(name = "photo")
    private String photo;

    @Column(name="date_created")
    private Date dateCreated;

    @Column(name="last_updated")
    private Date lastUpdated;

    @OneToMany(mappedBy = "course")
    private List<Chapter> chapters;

    @OneToMany(mappedBy = "course")
    private List<Evaluation> evaluations;
}
